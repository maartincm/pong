import cx_Freeze
import os.path
import os

executables = [cx_Freeze.Executable("main.py")]

include_files = [
    'Assets',
]

if os.name == 'nt':
    PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
    os.environ['TCL_LIBRARY'] = os.path.join(
        PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
    os.environ['TK_LIBRARY'] = os.path.join(
        PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')
    include_files.append(os.path.join(
        PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'))
    include_files.append(os.path.join(
        PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'))

cx_Freeze.setup(
    name="Simple Pong Game",
    version="1",
    options={
        "build_exe": {
            "packages": [
                "pyglet"
            ],
            "include_files": include_files,
        }
    },
    executables=executables,
    )
