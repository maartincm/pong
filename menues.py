from framework.menu import Menu, TextWidget
from framework.utils import valid_ip


class MultiPlayerMenu(Menu):

    def start(self):
        super(MultiPlayerMenu, self).start()
        self.addTitle('Assets/multiplayer.png', 0, 0, h_center=True)
        self.title.sprite.y = (self._window.height -
                               self._window.height / 8) - self.title.height
        self.addCenterButton('Assets/host.png', margine=20,
                             callback=self.host_game)
        self.addCenterButton('Assets/join.png', margine=20,
                             callback=self.join_game)
        self.click_stack = []
        self.selection = False

    def host_game(self):
        self.selection = 'host'

    def join_game(self):
        self.selection = 'join'


class JoinGameMenu(Menu):

    def start(self):
        super(JoinGameMenu, self).start()
        self.addBackGround('Assets/black_background.png')
        self.addTitle('Assets/title.png', 0, 0, h_center=True)
        self.title.sprite.y = (self._window.height -
                               self._window.height / 8) - self.title.height
        self.ip_input = TextWidget(
            '127.0.0.1', 400,
            int(self._window.height - self._window.height / 3),
            self._window.width - 800, self.batch, self.foreground)
        self.join_button = self.addCenterButton('Assets/join.png', margine=50,
                                                callback=self.join)
        self.widgets.append(self.ip_input)
        self.text_cursor = self._window.get_system_mouse_cursor('text')
        self.focus = None
        self.join = False
        self.set_focus(self.widgets[0])

    def join(self):
        ip = self.ip_input.document.text
        test = valid_ip(ip)
        if test:
            self.join = ip

    def set_focus(self, focus):
        if self.focus:
            self.focus.caret.visible = False
            self.focus.caret.mark = self.focus.caret.position = 0

        self.focus = focus
        if self.focus:
            self.focus.caret.visible = True
            self.focus.caret.mark = 0
            self.focus.caret.position = len(self.focus.document.text)

    def on_mouse_motion(self, x, y, dx, dy):
        for widget in self.widgets:
            if widget.hit_test(x, y):
                self._window.set_mouse_cursor(self.text_cursor)
                break
        else:
            self._window.set_mouse_cursor(None)

    def on_mouse_press(self, x, y, button, modifiers):
        super(JoinGameMenu, self).on_mouse_press(x, y, button, modifiers)
        for widget in self.widgets:
            if widget.hit_test(x, y):
                self.set_focus(widget)
                break
        else:
            self.set_focus(None)

        if self.focus:
            self.focus.caret.on_mouse_press(x, y, button, modifiers)

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        if self.focus:
            self.focus.caret.on_mouse_drag(x, y, dx, dy, buttons, modifiers)

    def on_text(self, text):
        if self.focus:
            self.focus.caret.on_text(text)

    def on_text_motion(self, motion):
        if self.focus:
            self.focus.caret.on_text_motion(motion)

    def on_text_motion_select(self, motion):
        if self.focus:
            self.focus.caret.on_text_motion_select(motion)


class MainMenu(Menu):

    def start(self):
        super(MainMenu, self).start()
        self.addBackGround('Assets/black_background.png')
        self.addTitle('Assets/title.png', 0, 0, h_center=True)
        self.title.sprite.y = (self._window.height -
                               self._window.height / 8) - self.title.height
        self.addCenterButton('Assets/solo_player.png', margine=20,
                             callback=self.start_solo)
        self.addCenterButton('Assets/multiplayer.png', margine=20,
                             callback=self.start_multi)
        self.click_stack = []
        self.selection = False

    def start_solo(self):
        self.selection = 'solo_player'

    def start_multi(self):
        self.selection = 'multiplayer'
