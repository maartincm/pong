import pyglet


class AudioPlayer(pyglet.media.Player):

    def __init__(self, init_sound=False, *args, **kwargs):
        super(AudioPlayer, self).__init__(*args, **kwargs)
        self.last_source = False
        if init_sound and isinstance(init_sound, (pyglet.media.Source,
                                                  pyglet.media.SourceGroup)):
            self.queue(init_sound)
            self.last_source = init_sound

    def queue(self, path=False, loop=False, stream=True, *args, **kwargs):
        if path:
            source = pyglet.media.load(path, streaming=stream)
            if loop:
                group = pyglet.media.SourceGroup(source.audio_format, None)
                group.queue(source)
                group.loop = True
                source = group
                self.looping = True
            super(AudioPlayer, self).queue(source, *args, **kwargs)
        else:
            super(AudioPlayer, self).queue(*args, **kwargs)

    def destroy(self):
        self.pause()
        self.delete()
