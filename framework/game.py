import pyglet
from pyglet.window import key

from .key_handler import KeyHandler
from .networking import App


class Image(object):

    def __init__(self, image, batch=False, group=False, x=0, y=0,
                 animation=None, centered=False):
        self.name = image.split('.')[0]
        self.image = pyglet.resource.image(image)
        if not batch:
            raise AttributeError(
                'Missing Batch when loading `%s`' % image)
        if not group:
            raise AttributeError(
                'Missing Group when loading `%s`' % image)
        self.width = self.image.width
        self.height = self.image.height
        if centered:
            self.image.anchor_x = self.width/2
            self.image.anchor_y = self.height/2
        self.sprite = pyglet.sprite.Sprite(self.image, x=x, y=y,
                                           batch=batch, group=group)
        self.animate = animation

    def getName(self):
        return self.name

    def getWidth(self):
        return self.width

    def getHeight(self):
        return self.height

    def getAnchor_x(self):
        return self.image.anchor_x

    def getAnchor_y(self):
        return self.image.anchor_y

    def getX(self):
        return self.sprite.x

    def getY(self):
        return self.sprite.y

    def deleteSprite(self):
        self.sprite.delete()

    def horizontal_center(self, max_width, dividend=2):
        self.sprite.x = max_width / dividend


class Scene(object):

    def __init__(self):
        self.key_handlers = []
        pass

    def disable_key_handlers(self):
        for handler in self.key_handlers:
            handler['disabled'] = True

    def start(self):
        pass

    def clear(self):
        pass

    def on_draw(self):
        pass

    def update(self, dt):
        pass

    def on_mouse_press(self, x, y, button, modifiers):
        pass

    def on_mouse_motion(self, x, y, dx, dy):
        pass

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        pass

    def on_text(self, text):
        pass

    def on_text_motion(self, motion):
        pass

    def on_text_motion_select(self, motion):
        pass


class Game(object):

    def __init__(self, default_scene=False, fps=60, resolution=False,
                 *args, **kwargs):
        self.fps = fps
        self.app = App(self)
        if default_scene and not isinstance(default_scene, Scene):
            err = ('A new Game instance should receive a Scene as parameter.' +
                   'Got %s instead') % default_scene
            raise Exception(err)
        self.resolution = resolution
        self.current_scene = default_scene
        if self.resolution:
            self.window = Window(self, width=self.resolution[0],
                                 height=self.resolution[1])
        else:
            self.window = Window(self)

    def startCurrentScene(self):
        self.current_scene._window = self.window
        self.current_scene.start()
        # self.current_scene._max_x = self.window.width
        # self.current_scene._max_y = self.window.height

    def clearCurrentScene(self):
        self.window.remove_handlers()
        self.current_scene.clear()
        del(self.current_scene)

    def nextScene(self, new_scene):
        self.current_scene = new_scene

    def update(self, dt):
        self.current_scene.update(dt)
        self.check_keys()

    def check_keys(self):
        if hasattr(self, 'key_handler'):
            self.key_handler.check()

    def run(self):
        self.keys = key.KeyStateHandler()
        self.key_handler = KeyHandler(self, self.keys)
        self.window.push_handlers(self.keys)
        if self.current_scene:
            self.current_scene.key_handlers = []
        self.startCurrentScene()
        pyglet.clock.schedule_interval(self.update, 1.0/self.fps)
        # pyglet.app.run()
        self.app.run()

    def stop(self):
        self.app.stop()


class Window(pyglet.window.Window):

    def __init__(self, game, *args, **kwargs):
        try:
            platform = pyglet.window.get_platform()
            display = platform.get_default_display()
        except AttributeError:
            display = pyglet.canvas.get_display()
        screen = display.get_default_screen()
        screen_width = screen.width
        screen_height = screen.height
        self.game = game
        super(Window, self).__init__(width=screen_width,
                                     height=screen_height,
                                     vsync=False,
                                     resizable=True,
                                     *args, **kwargs)

    def push_handlers(self, handler, *args, **kwargs):
        super(Window, self).push_handlers(handler, *args, **kwargs)
        handler['disabled'] = False
        self.game.current_scene.key_handlers.append(handler)

    def on_draw(self):
        self.clear()
        self.game.current_scene.on_draw()

    def on_mouse_press(self, x, y, button, modifiers):
        self.game.current_scene.on_mouse_press(x, y, button, modifiers)

    def on_mouse_motion(self, x, y, dx, dy):
        self.game.current_scene.on_mouse_motion(x, y, dx, dy)

    def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
        self.game.current_scene.on_mouse_drag(x, y, dx, dy, buttons, modifiers)

    def on_text(self, text):
        self.game.current_scene.on_text(text)

    def on_text_motion(self, motion):
        self.game.current_scene.on_text_motion(motion)

    def on_text_motion_select(self, motion):
        self.game.current_scene.on_text_motion_select(motion)
