from pyglet.window import key


class KeyHandler(object):

    def __init__(self, game, keys):
        self.game = game
        self.keys = keys
        self.toggling_fullscreen = False

    def toggle_fullscreen(self):
        if not self.toggling_fullscreen:
            self.toggling_fullscreen = True
            self.game.window.set_fullscreen(not self.game.window.fullscreen)

    def check(self):
        if self.keys[key.F11]:
            self.toggle_fullscreen()
        if self.toggling_fullscreen and not self.keys[key.F11]:
            self.toggling_fullscreen = False
        if self.keys[key.ESCAPE]:
            self.game.stop()
