import math
import ipaddress


def distance(p1, p2):
    distance = math.sqrt(((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2))
    return distance


def valid_ip(address):
    try:
        ipaddress.ip_address(address)
        return True
    except Exception:
        return False


def line_function(m, x, b=0):
    y = m * x + b
    return y
