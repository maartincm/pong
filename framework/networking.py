from .lib.pigtwist import pygletreactor
from twisted.protocols import basic

pygletreactor.install()
from twisted.internet import reactor, protocol, task, defer, endpoints  # noqa


class PongProtocol(basic.LineReceiver):
    delimiter = b'\n'

    def lineReceived(self, data):
        parsed_commands = data.decode('utf8').split('?')
        for command in parsed_commands:
            comm = command.split('$')
            if comm and len(comm) == 5 and \
                    comm[0] == '5':
                if comm[1] == 's':
                    ctype = 'state'
                if comm[1] == 'm':
                    ctype = 'move'
                self.factory.last_commands[comm[2]] = {
                    'type': ctype,
                    'position': (
                        float(comm[3].split(',')[0].strip('(')),
                        float(comm[3].split(',')[1].strip(')')),
                    ),
                    'velocity': (
                        float(comm[4].split(',')[0].strip('(')),
                        float(comm[4].split(',')[1].strip(')')),
                    ),
                }

    def connectionMade(self):
        super(PongProtocol, self).connectionMade()
        self.connected = True
        print('Connection Established!')

    def connectionLost(self, reason):
        super(PongProtocol, self).connectionLost()
        self.factory.connected -= 1


class ServerFactory(protocol.ServerFactory):
    command_stack = []
    last_commands = {}

    def __init__(self, *args, **kwargs):
        super(ServerFactory, self).__init__(*args, **kwargs)
        self.connected = 0
        self.connectedProtocols = []

    def buildProtocol(self, addr):
        print('Building Protocol')
        self.connectedProtocol = PongProtocol()
        self.connectedProtocols.append(self.connectedProtocol)
        self.connectedProtocol.factory = self
        self.connected += 1
        return self.connectedProtocol

    def clientConnectionFailed(self, connector, reason):
        print('Client Connection Failed')

    def clientConnectionLost(self, connector, reason):
        self.connected -= 1
        print('Client Connection Lost')


class ClientFactory(protocol.ClientFactory):
    command_stack = []
    last_commands = {}

    def __init__(self, *args, **kwargs):
        super(ClientFactory, self).__init__(*args, **kwargs)
        self.connected = 0

    def buildProtocol(self, addr):
        print('Building Protocol')
        self.connectedProtocol = PongProtocol()
        self.connectedProtocol.factory = self
        self.connected += 1
        return self.connectedProtocol

    def clientConnectionFailed(self, connector, reason):
        print('Client Connection Failed')

    def clientConnectionLost(self, connector, reason):
        self.connected -= 1
        print('Client Connection Lost')


class Server(object):

    def __init__(self, game, *args, **kwargs):
        self.fps = game.fps
        self.game = game
        self.reactor = reactor
        self.running = False

    def start_listening(self, port=35454):
        self.endpoint = endpoints.serverFromString(self.reactor,
                                                   'tcp:%s' % port)
        self.factory = ServerFactory()
        self.endpoint.listen(self.factory)
        self.loop = task.LoopingCall(self.netUpdate)
        self.loop.start(1.0/self.fps)

    def send(self, data):
        if hasattr(self.factory, 'connectedProtocols'):
            try:
                data = data.encode('utf8')
            except Exception:
                data = data
            for conn in self.factory.connectedProtocols:
                conn.sendLine(data)
            return True
        return False

    def netUpdate(self):
        # TODO
        if hasattr(self.factory, 'connectedProtocol'):
            pass


class Client(object):

    def __init__(self, game, *args, **kwargs):
        self.fps = game.fps
        self.game = game
        self.reactor = reactor
        self.running = False
        self.factory = ClientFactory()
        self.greeted = False

    def connect(self, ip, port=35454):
        self.reactor.connectTCP(str(ip), port, self.factory)
        self.loop = task.LoopingCall(self.netUpdate)
        self.loop_def = self.loop.start(1.0/self.fps)

    def error_handler(failure):
        print(failure)
        return

    def send(self, data):
        if hasattr(self.factory, 'connectedProtocol'):
            try:
                data = data.encode('utf8')
            except Exception:
                data = data
            self.factory.connectedProtocol.sendLine(data)
            return True
        return False

    def greet(self):
        if not self.greeted and self.send(b'Hello Server!'):
            self.greeted = True

    def netUpdate(self):
        self.greet()


class App(object):

    def __init__(self, game, *args, **kwargs):
        self.game = game
        self.fps = game.fps
        self.running = False
        self.server = Server(game)
        self.client = Client(game)
        self.reactor = reactor

    def run(self):
        self.running = True
        self.reactor.run()

    def stop(self):
        self.reactor.stop()
