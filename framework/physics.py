import pyglet
import math

from .utils import line_function


class PhysicalObject(pyglet.sprite.Sprite):

    def __init__(self, path=False, window=False, *args, **kwargs):
        self.img_path = pyglet.resource.image(path)
        super(PhysicalObject, self).__init__(
            img=self.img_path, *args, **kwargs)
        self.velocity_x, self.velocity_y = 0.0, 0.0
        self.velocity_delta = 0.0
        self.is_ground = False
        self.is_dead = False
        self._window = window
        self.mov_vector = False
        self.collide_with_vector = False
        self.vel_vector = (self.velocity_x, self.velocity_y)

    def __setattr__(self, name, value):
        super(PhysicalObject, self).__setattr__(name, value)
        if name in ['velocity_x', 'velocity_y'] and \
                hasattr(self, 'velocity_x') and \
                hasattr(self, 'velocity_y'):
            self.vel_vector = (self.velocity_x, self.velocity_y)

    def collides_with(self, obj):
        # TODO Finish Cases
        if self.collide_with_vector:
            if obj.collide_with_vector:
                face = 'top'
            else:
                face = self.vector_square_collision(obj)
        else:
            if obj.collide_with_vector:
                face = obj.vector_square_collision(self)
            else:
                face = 'top'
        return face

    def vector_square_collision(self, obj):
        """
        Self is Vector, Obj is Square
        """
        # TODO, Not generic
        s_width = obj.image.width
        s_height = obj.image.height
        s_x = obj.x
        s_y = obj.y
        if not self.mov_vector:
            return
        v_x_end = self.mov_vector[1][0]
        v_x_start = self.mov_vector[0][0]
        v_y_end = self.mov_vector[1][1]
        v_y_start = self.mov_vector[0][1]
        if (s_x <= v_x_end + v_x_start <= s_x + s_width) and \
                (s_y <= v_y_end + v_y_start <= s_y + s_height):
            # print('\n')
            # print('mod_vector: ', self.mov_vector)
            # print('left: ', s_x, 'vectorX: ', v_x_end + v_x_start, 'right: ',
            #       s_x + s_width, 'bot: ', s_y, 'vectorY: ',
            #       v_y_end + v_y_start, 'top: ', s_y + s_height)
            # print('vector square collision')
            if round(v_x_end - v_x_start, 4) == 0:
                if v_y_end < 0:
                    face = 'top'
                else:
                    face = 'bottom'
            else:
                m = v_y_end / v_x_end
                b = v_y_start + v_y_end - (m * (v_x_start + v_x_end))
                yr = line_function(m, s_x + s_width, b=b)
                yl = line_function(m, s_x, b=b)
                face = 'error'
                # print('yr: ', yr, 'yl: ', yl)
                if (s_y <= yr <= s_y + s_height) and v_x_end < 0:
                    face = 'right'
                if ((yr > s_y + s_height and yl < s_y + s_height) or
                        (yl > s_y + s_height and yr < s_y + s_height)) and \
                        v_y_end < 0:
                    face = 'top'
                if (s_y <= yl <= s_y + s_height) and v_x_end > 0:
                    face = 'left'
                if ((yr < s_y and yl > s_y) or
                        (yl < s_y and yr > s_y)) and \
                        v_y_end > 0:
                    face = 'bottom'
            # print('face: ', face)
            return face
        # elif (s_x <= v_x_start <= s_x + s_width) and \
        #         (s_y <= v_y_start - rad <= s_y + s_height):
        #     return 'bottom'
        # elif (s_x <= v_x_start <= s_x + s_width) and \
        #         (s_y <= v_y_start + rad <= s_y + s_height):
        #     return 'top'
        # elif (s_x <= v_x_start - rad <= s_x + s_width) and \
        #         (s_y <= v_y_start <= s_y + s_height):
        #     return 'right'
        # elif (s_x <= v_x_start + rad <= s_x + s_width) and \
        #         (s_y <= v_y_start <= s_y + s_height):
        #     return 'left'
        else:
            return False

    # def collides_with(self, obj):
    #     if (obj.x > self.x and obj.x < (self.x + self.image.width)) or \
    #             ((obj.x + obj.image.width) > self.x and
    #              obj.x < (self.x + self.image.width)):
    #         if (obj.y > self.y and obj.y < (self.y + self.image.height)) or \
    #                 ((obj.y + obj.image.height) > self.y and
    #                  obj.y < (self.y + self.image.height)):
    #             return True
    #     return False
    #
    #     max_x_dist = self.image.width / 2 + obj.image.width / 2
    #     x_dist = abs(self.x - obj.x)
    #     max_y_dist = self.image.height / 2 + obj.image.height / 2
    #     y_dist = abs(self.y - obj.y)
    #     return (x_dist <= max_x_dist) and (y_dist <= max_y_dist)

    def handle_collision_with(self, obj, face='top'):
        pass

    def update(self, dt):
        self.compute_radius()
        width = self.image.width
        height = self.image.height
        x_center = self.x + width / 2
        y_center = self.y + height / 2
        self.vel_vector = (self.velocity_x, self.velocity_y)
        vector = self.vel_vector
        norm = math.sqrt((vector[0] ** 2) + (vector[1] ** 2))
        if not norm:
            return False
        versor = (vector[0] / norm, vector[1] / norm)
        self.mov_vector = ((x_center, y_center),
                           (versor[0] * self.radius, versor[1] * self.radius))


class Circle(PhysicalObject):

    def __init__(self, center=False, radius=False, *args, **kwargs):
        super(Circle, self).__init__(*args, **kwargs)
        if not center:
            center = self.compute_center()
        if not radius:
            radius = self.compute_radius()
        self.center = center
        self.radius = radius

    def compute_center(self):
        center = (self.x + self.image.width / 2,
                  self.y + self.image.height / 2)
        return center

    def compute_radius(self):
        radius = min(self.image.width / 2, self.image.height / 2)
        return radius

    def __setattr__(self, name, value):
        if name in ['x', 'y']:
            self.center = self.compute_center()
        super(Circle, self).__setattr__(name, value)
