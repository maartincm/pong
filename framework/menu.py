from pyglet import graphics, text, gl
from pyglet.window import mouse

from .game import Scene, Image


class BasicRectangle(object):
    '''Draws a rectangle into a batch.'''

    def __init__(self, x1, y1, x2, y2, batch, group):
        self.vertex_list = batch.add(
            4, gl.GL_QUADS, group,
            ('v2i', [x1, y1, x2, y1, x2, y2, x1, y2]),
            ('c4B', [200, 200, 220, 255] * 4)
        )


class TextWidget(object):

    def __init__(self, init_text, x, y, width, batch, group):
        self.batch = batch
        self.group = group
        self.document = text.document.UnformattedDocument(init_text)
        self.document.set_style(
            0, len(self.document.text),
            dict(color=(0, 0, 0, 255))
        )
        font = self.document.get_font()
        height = font.ascent - font.descent

        self.layout = text.layout.IncrementalTextLayout(
            self.document, width, height, multiline=False, batch=batch,
            group=group)
        self.caret = text.caret.Caret(self.layout)

        self.layout.x = x
        self.layout.y = y

        # Rectangular outline
        pad = 2
        self.rectangle = BasicRectangle(x - pad, y - pad,
                                        x + width + pad, y + height + pad,
                                        batch, group)

    def hit_test(self, x, y):
        return (0 < x - self.layout.x < self.layout.width and
                0 < y - self.layout.y < self.layout.height)


class Button(Image):

    def __init__(self, *args, **kwargs):
        if 'callback' in kwargs:
            callback = kwargs.pop('callback')
        super(Button, self).__init__(*args, **kwargs)
        if not callback:
            raise Exception('Callback should be set for a button')
        self.callback = callback

    def handle_click(self):
        if not self.callback:
            return
        self.callback()

    def isAbove(self, x, y):
        leftBoundary = self.getX() - self.getAnchor_x()
        rightBoundary = self.getX() + self.getAnchor_x()
        bottomBoundary = self.getY() - self.getAnchor_y()
        topBoundary = self.getY() + self.getAnchor_y()
        return ((leftBoundary <= x <= rightBoundary) and
                (bottomBoundary <= y <= topBoundary))


class Title(Image):

    def __init__(self, *args, **kwargs):
        super(Title, self).__init__(*args, **kwargs)


class Menu(Scene):
    def __init__(self, menuWidth, menuHeight, x=0, y=0, *args, **kwargs):
        '''Menu Width: width of menu
           Menu Height: height of menu
           Used as relative positioning for buttons
           x: Position of menu Horizontally inside of the window
           y: Position of menu Vertically inside of the window

           buttonShift: the shift of buttons from mid height'''
        super(Menu, self).__init__(*args, **kwargs)
        self.widgets = []
        self.buttons = []
        self.click_stack = []
        self.batch = graphics.Batch()
        self.background = graphics.OrderedGroup(0)
        self.foreground = graphics.OrderedGroup(1)
        self.title = False
        self.background_image = False
        self.width = menuWidth
        self.height = menuHeight
        self.x = x
        self.y = y
        self.buttonShift = 0

    def start(self):
        super(Menu, self).start()
        self.drawables = [self.batch]

    def update(self, dt):
        super(Menu, self).update(dt)
        last_click = False
        if self.click_stack:
            last_click = self.click_stack.pop()
        for btn in self.buttons:
            if last_click and btn.isAbove(last_click['x'], last_click['y']):
                btn.handle_click()

    def on_mouse_press(self, x, y, button, modifiers):
        if mouse.LEFT:
            self.click_stack.append({
                'x': x,
                'y': y,
                'button': button,
                'mod': modifiers,
            })

    def addBackGround(self, image):
        self.background_image = Image(image, x=self.x, y=self.y,
                                      batch=self.batch, group=self.background)

    def addTitle(self, image, xPos, yPos, h_center=False, v_center=False):
        self.title = Title(image, x=xPos + self.x, y=yPos + self.y,
                           batch=self.batch, group=self.foreground,
                           centered=True)
        if h_center:
            self.title.horizontal_center(self._window.width,
                                         2 if h_center is True else h_center)

    def addButton(self, image, xPos, yPos, callback=False):
        self.buttons.append(Button(image, x=xPos + self.x,
                                   y=yPos + self.y, batch=self.batch,
                                   group=self.foreground, centered=True,
                                   callback=callback))

    def addCenterButton(self, image, margine=0, callback=False):
        self.addButton(image, self.width/2 + self.x,
                       self.height/2 + self.y + self.buttonShift,
                       callback=callback)
        self.buttonShift -= self.buttons[-1].getHeight() + margine

    def getButton(self, name):
        for button in self.buttons:
            if button.getName() == name:
                return button

    def ifAbove(self, name, x, y):
        return self.getButton(name).ifAbove(x, y)

    def on_draw(self):
        for obj in self.drawables:
            obj.draw()

    def clear(self):
        if self.background_image:
            self.background_image.deleteSprite()
        if self.title:
            self.title.deleteSprite()
        if self.buttons:
            for button in self.buttons:
                button.deleteSprite()
