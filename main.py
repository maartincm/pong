from framework.game import Game, Scene, Image, Window
from framework.audio import AudioPlayer
from framework.utils import distance
from framework.physics import Circle, PhysicalObject
from framework.networking import Client, Server

from menues import MainMenu, MultiPlayerMenu, JoinGameMenu

import pyglet
from pyglet.window import key

FPS = 120


class Player(PhysicalObject):

    def __init__(self, up=key.UP, down=key.DOWN, left=key.LEFT,
                 right=key.RIGHT, pos='left', *args, **kwargs):
        self.path = 'Assets/bar.png'
        super(Player, self).__init__(path=self.path, *args, **kwargs)
        self.keys = key.KeyStateHandler()
        self._window.push_handlers(self.keys)
        self.velocity_y = 600
        self.velocity_x = 400
        self.is_player = True
        self.up = up
        self.down = down
        self.left = left
        self.right = right
        if (pos == 'left'):
            self.max_x_dir = 1
        elif (pos == 'right'):
            self.max_x_dir = -1
        self.max_x_movement = 400
        self.initial_x_pos = self.x
        self.moving = False
        self.following = False
        self.to_x = self.x
        self.to_y = self.y

    def move_up(self, dt):
        if self.y <= self._window.height - self.image.height:
            self.y += self.velocity_y * dt
            self.moving = ('y', self.velocity_y * dt)

    def move_down(self, dt):
        if self.y >= 0:
            self.y -= self.velocity_y * dt
            self.moving = ('y', self.velocity_y * dt * -1)

    def move_left(self, dt):
        if self.max_x_dir == 1 and self.initial_x_pos < self.x:
            self.x -= self.velocity_x * dt
            self.moving = ('x', self.velocity_x * dt * -1)
        elif self.max_x_dir == -1 and \
                self.initial_x_pos - self.max_x_movement < self.x:
            self.x -= self.velocity_x * dt
            self.moving = ('x', self.velocity_x * dt * -1)

    def move_right(self, dt):
        if self.max_x_dir == 1 and self.max_x_movement > self.x:
            self.x += self.velocity_x * dt
            self.moving = ('x', self.velocity_x * dt)
        elif self.max_x_dir == -1 and self.initial_x_pos > self.x:
            self.x += self.velocity_x * dt
            self.moving = ('x', self.velocity_x * dt)

    def update(self, dt):
        if self.following:
            if (abs(self.to_x - self.x) > 10):
                self.x += ((self.to_x - self.x) / abs(self.to_x - self.x)) * \
                    self.velocity_x * dt
            if (abs(self.to_y - self.y) > 10):
                self.y += ((self.to_y - self.y) / abs(self.to_y - self.y)) * \
                    self.velocity_y * dt
        if self.keys['disabled']:
            return
        if self.keys[self.up]:
            self.move_up(dt)

        if self.keys[self.down]:
            self.move_down(dt)

        if self.keys[self.left]:
            self.move_left(dt)

        if self.keys[self.right]:
            self.move_right(dt)

        if not (self.keys[self.right] or self.keys[self.left] or
                self.keys[self.up] or self.keys[self.down]):
            self.moving = False


class Ball(Circle):

    def __init__(self, *args, **kwargs):
        path = 'Assets/ball.png'
        self.sync = False
        super(Ball, self).__init__(path=path, *args, **kwargs)
        self.velocity_x = 600
        self.velocity_y = 700
        self.velocity_delta = 30
        self.out = False
        self.collided = False
        self.collide_with_vector = True
        self.following = False
        self.to_x = self.x
        self.to_y = self.y
        self.bounced = False
        self.last_bounce = False

    def update(self, dt):
        super(Ball, self).update(dt)
        if hasattr(self, 'following') and self.following and self.last_bounce:
            self.x = self.last_bounce['position'][0]
            self.y = self.last_bounce['position'][1]
            self.velocity_x = self.last_bounce['velocity'][0]
            self.velocity_y = self.last_bounce['velocity'][1]
            self.sync = True
            self.last_bounce = False
        if self.following and not self.sync:
            return
        self.x += self.velocity_x * dt
        self.y += self.velocity_y * dt
        self.out = self.check_bounds()

    def check_bounds(self):
        min_x = 0
        min_y = 0
        max_x = self._window.width - self.image.width
        max_y = self._window.height - self.image.height
        if self.x <= min_x + 5:
            return 'left'
        elif self.x >= max_x - 5:
            return 'right'
        if self.y <= min_y:
            self.velocity_y = abs(self.velocity_y)
        elif self.y >= max_y:
            self.velocity_y = abs(self.velocity_y) * -1
        return False

    def handle_collision_with(self, obj, face='top'):
        if self.collided:
            if distance(self.collided, (self.x, self.y)) > 300:
                self.collided = False
        if obj.is_player and not self.collided:
            speed_y = abs(self.velocity_y)
            speed_x = abs(self.velocity_x)
            boost_x = 0.0
            boost_y = 0.0
            if obj.moving:
                if obj.moving[0] == 'x':
                    boost_x = abs(obj.moving[1] * 30)
                elif obj.moving[0] == 'y':
                    boost_y = abs(obj.moving[1] * 30)
            if face in ['top', 'bottom']:
                if not speed_y:
                    return
                self.velocity_y = (
                    (self.velocity_y / speed_y) * -1 * speed_y) + \
                    self.velocity_delta + boost_y
                self.velocity_x = (self.velocity_x / speed_x) * \
                    speed_x + boost_x
                self.bounced = True
            elif face in ['left', 'right']:
                if not speed_x:
                    return
                self.velocity_x = (
                    (self.velocity_x / speed_x) *
                    (speed_x + self.velocity_delta + boost_x - boost_y)) * -1
                self.velocity_y = (self.velocity_y / speed_y) * \
                    (speed_y + boost_y - boost_x)
                self.bounced = True
            self.collided = (self.x, self.y)


class BouncingBall(Scene):

    def __init__(self, mode, *args, **kwargs):
        super(BouncingBall, self).__init__(*args, **kwargs)
        self.mode = mode

    def start(self):
        super(BouncingBall, self).start()
        self.game_ended = False
        self.restart_game = False
        self.quit_game = False
        self.waiting = False
        self.other_player_ready = False
        self.restart_sent = False
        self.game_objects = []
        self.batch = pyglet.graphics.Batch()
        self.drawables = [self.batch]
        self.background = pyglet.graphics.OrderedGroup(0)
        self.foreground = pyglet.graphics.OrderedGroup(1)
        self.bg_image = Image('Assets/black_background.png',
                              group=self.background,
                              batch=self.batch)
        self.ball = Ball(x=500, y=500, window=self._window,
                         batch=self.batch,
                         group=self.foreground)
        self.game_objects.append(self.ball)
        self.player1 = Player(x=25, y=500, window=self._window,
                              batch=self.batch,
                              group=self.foreground,
                              up=key.W, down=key.S,
                              left=key.A, right=key.D,
                              pos='left')
        self.game_objects.append(self.player1)
        if self.mode == 'solo':
            self.player2 = Player(y=500, window=self._window,
                                  batch=self.batch,
                                  group=self.foreground,
                                  up=key.UP, down=key.DOWN,
                                  left=key.LEFT, right=key.RIGHT,
                                  pos='right')
            self.player2.x = self._window.width - self.player2.image.width - 25
            self.player2.initial_x_pos = self.player2.x
            self.game_objects.append(self.player2)
        if self.mode == 'host':
            self.waiting_label = pyglet.text.Label('Waiting for Player',
                                                   font_size=40,
                                                   batch=self.batch,
                                                   font_name='Times New Roman',
                                                   group=self.foreground,
                                                   y=self._window.height - 200)
            self.waiting_label.x = (self._window.width / 2) - \
                (self.waiting_label.content_width / 2)
            self.ball.velocity_x = 0
            self.ball.velocity_y = 0
            self.disable_key_handlers()
            self.waiting = True
        if self.mode == 'join':
            self.disable_key_handlers()
            self.player2 = Player(y=500, window=self._window,
                                  batch=self.batch,
                                  group=self.foreground,
                                  up=key.UP, down=key.DOWN,
                                  left=key.LEFT, right=key.RIGHT,
                                  pos='right')
            self.player2.x = self._window.width - self.player2.image.width - 25
            self.player2.initial_x_pos = self.player2.x
            self.player1.following = True
            self.ball.following = True
            self.game_objects.append(self.player2)
        self.start_audio_player()

    def start_audio_player(self):
        self.audio_player = AudioPlayer()
        self.audio_player.queue(path='Assets/ludovico.mp3', loop=True)
        # self.audio_player.play()

    def create_end_label(self, winner):
        self.winner_label = pyglet.text.Label('Player %s Won! Yeah!' % winner,
                                              font_name='Times New Roman',
                                              font_size=40, batch=self.batch,
                                              group=self.foreground,
                                              y=self._window.height - 200)
        self.winner_label.x = (self._window.width / 2) - \
            (self.winner_label.content_width / 2)
        if self.mode in ['host', 'solo']:
            self.restart_label = pyglet.text.Label('Press R to restart game ' +
                                                   'or Q to quit',
                                                   font_name='Times New Roman',
                                                   group=self.foreground,
                                                   font_size=18,
                                                   batch=self.batch)
            self.restart_label.x = (self._window.width / 2) - \
                (self.restart_label.content_width / 2)
            self.restart_label.y = (self.winner_label.y -
                                    self.restart_label.content_height - 20)
        elif self.mode in ['join']:
            self.waiting_label = pyglet.text.Label('Waiting for Host',
                                                   font_name='Times New Roman',
                                                   group=self.foreground,
                                                   font_size=18,
                                                   batch=self.batch)
            self.waiting_label.x = (self._window.width / 2) - \
                (self.waiting_label.content_width / 2)
            self.waiting_label.y = (self.winner_label.y -
                                    self.waiting_label.content_height - 20)

    def end_game(self):
        self.game_ended = True
        for obj in self.game_objects:
            obj.velocity_x = 0
            obj.velocity_y = 0
        if self.ball.out == 'left':
            winner = '2'
        elif self.ball.out == 'right':
            winner = '1'
        else:
            winner = 'Zero'
        self.create_end_label(winner)
        self.disable_key_handlers()
        if self.mode in ['host', 'solo']:
            self.restart_keys = key.KeyStateHandler()
            self._window.push_handlers(self.restart_keys)

    def restart(self):
        self.winner_label.delete()
        if self.mode in ['host', 'solo']:
            self.restart_label.delete()
        elif self.mode in ['join']:
            self.waiting_label.delete()
        self.restart_game = True
        self.game_ended = False

    def quit(self):
        self.winner_label.delete()
        self.restart_label.delete()
        self.quit_game = True

    def update(self, dt):
        if self.game_ended:
            if self.mode in ['host', 'solo']:
                if self.restart_keys[key.R]:
                    self.restart()
                if self.restart_keys[key.Q]:
                    self.quit()
        for obj in self.game_objects:
            obj.update(dt)
        if self.ball.out:
            self.end_game()

        for i in range(len(self.game_objects)):
            for j in range(i+1, len(self.game_objects)):
                obj_1 = self.game_objects[i]
                obj_2 = self.game_objects[j]
                face = obj_1.collides_with(obj_2)
                if face:
                    obj_1.handle_collision_with(obj_2, face)
                    obj_2.handle_collision_with(obj_1, face)

        self.check_audio_playing()

    def check_audio_playing(self):
        if hasattr(self, 'audio_player'):
            if not self.audio_player.playing:
                del(self.audio_player)
                self.start_audio_player()

    def clear(self):
        super(BouncingBall, self).clear()
        self.audio_player.destroy()
        for obj in self.game_objects:
            obj.delete()

    def on_draw(self):
        for obj in self.drawables:
            obj.draw()


class PongGame(Game):

    def __init__(self, *args, **kwargs):
        super(PongGame, self).__init__(*args, **kwargs)

    def update(self, dt):
        super(PongGame, self).update(dt)
        scene = self.current_scene
        if isinstance(scene, BouncingBall):
            if scene.restart_game:
                if scene.mode == 'host' and not scene.restart_sent:
                    command = '5$s$restart$(0,0)$(0,0)'
                    self.app.server.send(command)
                    scene.restart_sent = True
                if scene.mode == 'host' and not scene.other_player_ready:
                    return
                if scene.mode == 'join':
                    command = '5$s$restart_ready$(0,0)$(0,0)'
                    self.app.client.send(command)
                current_mode = self.current_scene.mode
                self.clearCurrentScene()
                self.nextScene(BouncingBall(mode=current_mode))
                self.startCurrentScene()
            if scene.quit_game:
                # TODO Quick method to swap
                self.clearCurrentScene()
                self.nextScene(MainMenu(self.window.width,
                                        self.window.height))
                self.startCurrentScene()
            if scene.mode == 'host':
                if self.app.server.factory.connected == 1 and scene.waiting:
                    scene.waiting = False
                    scene.waiting_label.delete()
                    self.window.push_handlers(scene.player1.keys)
                    scene.player2 = Player(y=500, window=scene._window,
                                           batch=scene.batch,
                                           group=scene.foreground,
                                           pos='right')
                    scene.player2.keys['disabled'] = True
                    scene.player2.x = scene._window.width - \
                        scene.player2.image.width - 25
                    scene.player2.initial_x_pos = scene.player2.x
                    scene.player2.following = True
                    scene.player2.to_x = scene.player2.x
                    scene.player2.to_y = scene.player2.y
                    scene.game_objects.append(scene.player2)
                    scene.ball.velocity_x = 60
                    scene.ball.velocity_y = 70
                    command = ''
                    pos = (scene.ball.x, scene.ball.y)
                    vel = scene.ball.vel_vector
                    command = '5$s$ball$%s$%s' % (pos, vel)
                    scene.ball.bounced = False
                    # pos = (scene.ball.x, scene.ball.y)
                    # vel = scene.ball.mov_vector
                    # command = '5$s$ball$%s$%s' % (pos, vel)
                    pos = (scene.player1.x, scene.player1.y)
                    vel = scene.player1.vel_vector
                    command += '?5$m$p1$%s$%s' % (pos, vel)
                    self.app.server.send(command)
                elif self.app.server.factory.connected == 1:
                    command = ''
                    if scene.ball.bounced:
                        pos = (scene.ball.x, scene.ball.y)
                        vel = scene.ball.vel_vector
                        command = '5$s$ball$%s$%s' % (pos, vel)
                        scene.ball.bounced = False
                    pos = (scene.player1.x, scene.player1.y)
                    vel = scene.player1.vel_vector
                    command += '?5$m$p1$%s$%s' % (pos, vel)
                    self.app.server.send(command)
            if scene.mode == 'join':
                if self.app.client.factory.connected == 1:
                    pos = (scene.player2.x, scene.player2.y)
                    vel = scene.player2.vel_vector
                    self.app.client.send(
                        '5$m$p2$%s$%s' % (pos, vel)
                    )
                    # commands = self.app.client.factory.command_stack
                    # ball = False
                    # p1 = False
                    # for com in commands:
                    #     if not ball and com['entity'] == 'ball':
                    #         scene.ball.x = float(com['position'][0])
                    #         scene.ball.y = float(com['position'][1])
                    #         ball = True
                    #     if not p1 and com['entity'] == 'p1':
                    #         scene.player1.x = float(com['position'][0])
                    #         scene.player1.y = float(com['position'][1])
                    #         p1 = True
                    #     if ball and p1:
                    #         break
                    # self.app.client.factory.command_stack = []

        if isinstance(scene, MainMenu) and \
                scene.selection:
            selection = scene.selection
            if selection == 'solo_player':
                self.clearCurrentScene()
                self.nextScene(BouncingBall(mode='solo'))
                self.startCurrentScene()
            if selection == 'multiplayer':
                self.clearCurrentScene()
                self.nextScene(MultiPlayerMenu(self.window.width,
                                               self.window.height))
                self.startCurrentScene()
        if isinstance(scene, MultiPlayerMenu):
            if scene.selection == 'host':
                self.host_game()
            elif scene.selection == 'join':
                self.clearCurrentScene()
                self.nextScene(JoinGameMenu(self.window.width,
                                            self.window.height))
                self.startCurrentScene()
        if isinstance(scene, JoinGameMenu):
            if scene.join:
                self.join_game(scene.join)

    def host_game(self):
        print('hosting!')
        self.clearCurrentScene()
        self.nextScene(BouncingBall(mode='host'))
        self.startCurrentScene()
        self.app.server.start_listening()
        # self.app.client.connect('127.0.0.1')

    def join_game(self, ip):
        print('joining %s!' % ip)
        self.clearCurrentScene()
        self.nextScene(BouncingBall(mode='join'))
        self.startCurrentScene()
        self.app.client.connect(ip)


def CnetUpdate(obj):
    commands = obj.factory.last_commands
    scene = obj.game.current_scene
    for entity, command in commands.items():
        if entity == 'ball':
            scene.ball.last_bounce = command
        elif entity == 'p1':
            scene.player1.to_x = command['position'][0]
            scene.player1.to_y = command['position'][1]
        elif entity == 'restart':
            scene.restart_game = True
    obj.factory.last_commands = {}


Client.netUpdate = CnetUpdate


def SnetUpdate(obj):
    commands = obj.factory.last_commands
    scene = obj.game.current_scene
    for entity, command in commands.items():
        if entity == 'p2':
            scene.player2.to_x = command['position'][0]
            scene.player2.to_y = command['position'][1]
        elif entity == 'restart_ready':
            scene.other_player_ready = True
    obj.factory.last_commands = {}


Server.netUpdate = SnetUpdate


if __name__ == '__main__':
    game = PongGame(fps=FPS)
    fps_display = pyglet.window.FPSDisplay(game.window)
    wind_draw = Window.on_draw

    def on_draw(obj):
        wind_draw(obj)
        fps_display.draw()

    Window.on_draw = on_draw

    game.current_scene = MainMenu(game.window.width,
                                  game.window.height)
    game.run()
